(function(window, undefined) {
  'use strict';

  var class2type = {};
  var toString = class2type.toString;
  var hasOwn = class2type.hasOwnProperty;

  var core = {
    onReady: function(fnc) {
        if (document.readyState === 'complete' || (document.readyState !== 'loading' && !document.documentElement.doScroll)) {
            fnc();
        } else {
            document.addEventListener('DOMContentLoaded', fnc, false);
        }
    },
    toType: function(obj) {
        if (core.isNull(obj)) {
            return obj + "";
        }

        return typeof obj === "object" || typeof obj === "function"
            ? class2type[toString.call(obj)] || "object"
            : typeof obj;
    },
    not: function(obj) {
        return !obj;
    },
    isNull: function(obj) {
        return obj === null;
    },
    isNotNull: function(obj) {
        return core.not(core.isNull(obj));
    },
    isUndefined: function(obj) {
        return obj === undefined;
    },
    isNotUndefined: function(obj) {
        return core.not(core.isUndefined(obj));
    },
    isFunction: function(obj) {
        return typeof obj === "function" && typeof obj.nodeType !== "number";
    },
    isWindow: function(obj) {
        return core.isNotNull(obj) && obj === obj.window;
    }
  };

  window.core = core;
})(window);